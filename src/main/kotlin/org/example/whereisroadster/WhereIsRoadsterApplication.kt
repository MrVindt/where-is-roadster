package org.example.whereisroadster

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WhereIsRoadsterApplication

fun main(args: Array<String>) {
    runApplication<WhereIsRoadsterApplication>(*args)
}

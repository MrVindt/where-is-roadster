package org.example.whereisroadster.client.http

import org.springframework.http.ResponseEntity

interface HttpClient<T> {

    fun get(): ResponseEntity<T>
}
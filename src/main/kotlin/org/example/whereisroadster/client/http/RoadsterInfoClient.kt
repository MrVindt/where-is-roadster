package org.example.whereisroadster.client.http

import org.example.whereisroadster.dto.RoadsterInfo
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestClient

@Service
class RoadsterInfoClient(
    private val restClient: RestClient,
    @Value("\${spacex.http.get.roadsterInfo}") private val roadsterInfoUri: String
): HttpClient<RoadsterInfo> {


    override fun get() =
        restClient
        .get()
        .uri(roadsterInfoUri)
        .retrieve()
        .toEntity(RoadsterInfo::class.java)

}
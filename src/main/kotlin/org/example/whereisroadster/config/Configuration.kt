package org.example.whereisroadster.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestClient

@Configuration
class Configuration {

    @Bean
    fun restClient() = RestClient.create()

}
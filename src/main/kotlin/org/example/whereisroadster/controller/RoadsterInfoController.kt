package org.example.whereisroadster.controller

import org.example.whereisroadster.client.http.HttpClient
import org.example.whereisroadster.dto.RoadsterInfo
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/roadster")
class RoadsterInfoController(
    val httpClient: HttpClient<RoadsterInfo>
) {

    @GetMapping("info")
    fun getRoadsterInfo() = httpClient.get()

}
package org.example.whereisroadster.dto

data class RoadsterInfo(
    val earthDistanceKm: String? = null,
    val marsDistanceKm: String? = null,
    val speedKph: String? = null,
    val wikipedia: String? = null,
    val video: String? = null,
    val details: String? = null
)
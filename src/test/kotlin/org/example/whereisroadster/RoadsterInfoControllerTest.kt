package org.example.whereisroadster

import org.example.whereisroadster.client.http.HttpClient
import org.example.whereisroadster.controller.RoadsterInfoController
import org.example.whereisroadster.dto.RoadsterInfo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@WebMvcTest(RoadsterInfoController::class)
class RoadsterInfoControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var httpClient: HttpClient<RoadsterInfo>

    @Test
    fun roadsterInfoTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/roadster/info"))
            .andExpect(MockMvcResultMatchers.status().isOk)
    }

}